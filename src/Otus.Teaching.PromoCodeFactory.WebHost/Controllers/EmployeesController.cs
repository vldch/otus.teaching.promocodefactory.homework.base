﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;

        public EmployeesController(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }

        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IEnumerable<Employee>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();
            return employees;
        }


        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();

            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };
            return employeeModel;
        }

        /// <summary>
        /// Создать нового сотрудника
        /// </summary>
        /// <param name="createEmployee"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<Employee>> Create()
        {
            var allEmployees = await _employeeRepository.GetAllAsync();
            var employee = allEmployees.First();
            return await _employeeRepository.Create(employee);
        }

        /// <summary>
        /// Удалить сотрудника
        /// </summary>
        /// <param name="createEmployee"></param>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task Delete(Guid id)
        {
            await _employeeRepository.Delete(id);
            return;
        }
        /// <summary>
        /// Обновить сотрудника
        /// </summary>
        /// <param name="UpdateEmployee"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task Update(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);
            if (employee == null)
                throw new ArgumentNullException("С таким Id сотрудник не найден!");
            employee.Roles.Add(new Role() {Id =Guid.NewGuid(), Name="СозданоАвтоматически" });
            await _employeeRepository.Update(employee);
        }
    }
}